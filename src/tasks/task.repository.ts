import { Task } from "./task.entity";
import { Repository, EntityRepository } from "typeorm";
import { GetTasksFilterDto } from "./dto/get-task-filter.dto";
import { User } from "src/auth/user.entity";
import { InternalServerErrorException, Logger } from "@nestjs/common";
import { CreateTaskDto } from "./dto/create-task.dto";
import { TaskStatus } from "./task-status.enum";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>{
    private logger = new Logger("TaskRepository");
    async getTasks(
        filerDto: GetTasksFilterDto,
        user: User): Promise<Task[]> {
        const { status, search } = filerDto;
        const query = this.createQueryBuilder('task');

        query.where('task.userId = :userId', { userId: user.id });
        if (status) {
            query.andWhere('task.status = :status', { status });
        }
        if (search) {
            query.andWhere('task.title LIKE :search OR task.description LIKE :search', { search: `%${search}%` });
        }

        try {
            const tasks = await query.getMany();
            return tasks;
        } catch (err) {
            this.logger.error(`Failed to get tasks for user "${user.username}". Filters: ${JSON.stringify(filerDto)}`)
            throw new InternalServerErrorException()
        }

    }

    async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
        const { title, description } = createTaskDto;
        const task = new Task();
        task.title = title;
        task.description = description;
        task.status = TaskStatus.OPEN;
        task.user = user;

        try {
            await task.save();
        } catch (error) {
            this.logger.error(`Failed to create a task for user "${user.username}" Data: ${createTaskDto}`, error.stack)
        }
        delete task.user;
        return task;
    }
}